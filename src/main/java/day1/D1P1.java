package day1;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class D1P1
{
    public static void main(String[] args) throws IOException {
        List<Integer> data = IOUtils.readLines(new FileReader("./src/main/java/day1/input.txt")).stream().map(Integer::parseInt).collect(Collectors.toList());

        System.out.println(data.stream().reduce(0, Integer::sum));
    }
}
