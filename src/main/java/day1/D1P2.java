package day1;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class D1P2
{
    public static void main(String[] args) throws IOException {
        List<Integer> data = IOUtils.readLines(new FileReader("./src/main/java/day1/input.txt")).stream().map(Integer::parseInt).collect(Collectors.toList());

        Set<Integer> freq = new HashSet<>();
        int currElem = 0;
        int currFreq = 0;

        for (; true; currElem++)
        {
            if (currElem >= data.size())
                currElem -= data.size();

            currFreq += data.get(currElem);

            if (freq.contains(currFreq))
            {
                System.out.println(currFreq);
                return;
            }

            freq.add(currFreq);
        }
    }
}
