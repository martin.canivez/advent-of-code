package day6;

import javafx.util.Pair;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Author: Martin
 * created on 06/12/2018.
 */
public class D6P1
{
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day6/input.txt")));
        
        List<Pair<Integer, Integer>> coord = data.stream()
                                                 .map(s -> new Pair<>(Integer.parseInt(s.split(", ")[0]),
                                                                      Integer.parseInt(s.split(", ")[1])))
                                                 .collect(Collectors.toList());
        
        Map<Integer, Integer> area = new HashMap<>();
    
        final int[] currElem = {0};
        Map<Integer, Pair<Integer, Integer>> refs = new HashMap<>();
        
        coord.forEach(c -> {
            refs.put(currElem[0], c);
            currElem[0]++;
        });
        
        Set<Integer> pieces = new HashSet<>(refs.keySet());
        
        int xMin = coord.parallelStream().min(Comparator.comparingInt(Pair::getKey)).orElseThrow(RuntimeException::new).getKey()-1;
        int xMax = coord.parallelStream().max(Comparator.comparingInt(Pair::getKey)).orElseThrow(RuntimeException::new).getKey()+1;
        int yMin = coord.parallelStream().min(Comparator.comparingInt(Pair::getValue)).orElseThrow(RuntimeException::new).getValue()-1;
        int yMax = coord.parallelStream().max(Comparator.comparingInt(Pair::getValue)).orElseThrow(RuntimeException::new).getValue()+1;
        
        for (int i = xMin; i <= xMax; i++)
        {
            
            for (int j = yMin; j <= yMax; j++)
            {
                int closest = closestElem(new Pair<>(i, j), refs);
                
                if (closest < 0)
                    continue;
                
                area.put(closest, area.getOrDefault(closest, 0) + 1);
                
                if (i == xMin || i == xMax || j == yMin || j == yMax)
                    pieces.remove(closest);
            }
        }
        
        area = area.entrySet()
                   .stream()
                   .filter(e -> pieces.contains(e.getKey()))
                   .collect(Collectors.toMap(Map.Entry::getKey , Map.Entry::getValue, (a,b) -> a, TreeMap::new));
        
        area.entrySet()
            .stream()
            .sorted((e1, e2) -> -e1.getValue().compareTo(e2.getValue()))
            .limit(1)
            .forEach(e -> System.out.println(e.getValue()));
    }
    
    static Integer closestElem(Pair<Integer, Integer> currCoordinate, Map<Integer, Pair<Integer, Integer>> elemMap)
    {
        int closest = -1;
        int closestDistance = Integer.MAX_VALUE;
        boolean conflict = false;
        
        for (Map.Entry<Integer, Pair<Integer, Integer>> elem : elemMap.entrySet())
        {
            Pair<Integer, Integer> elemCoords = elem.getValue();
            int distance = Math.abs(elemCoords.getKey() - currCoordinate.getKey()) +
                           Math.abs(elemCoords.getValue() - currCoordinate.getValue());
            
            if (distance < closestDistance)
            {
                closestDistance = distance;
                conflict = false;
                closest = elem.getKey();
            }
            else if (distance == closestDistance)
                conflict = true;
        }
        
        return conflict ? -1 : closest;
    }
}
