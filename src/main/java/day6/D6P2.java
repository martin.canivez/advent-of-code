package day6;

import javafx.util.Pair;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Author: Martin
 * created on 06/12/2018.
 */
public class D6P2
{
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day6/input.txt")));
    
        Set<Pair<Integer, Integer>> coord = data.stream()
                                                .map(s -> new Pair<>(Integer.parseInt(s.split(", ")[0]),
                                                                     Integer.parseInt(s.split(", ")[1])))
                                                .collect(Collectors.toSet());

        int radius = 1000;

        int xMid = coord.parallelStream().map(Pair::getKey).reduce(Integer::sum).orElseThrow(RuntimeException::new)/coord.size();
        int yMid = coord.parallelStream().map(Pair::getValue).reduce(Integer::sum).orElseThrow(RuntimeException::new)/coord.size();
        
        int result = 0;

        for (int i = xMid-radius; i <= xMid+radius; i++)
        {

            for (int j = yMid-(radius-Math.abs(i-xMid)); j <= yMid+(radius-Math.abs(i-xMid)); j++)
            {
                int totalDist = distToAll(new Pair<>(i, j), coord);

                if (totalDist < 10000)
                    result++;
            }
        }
    
        System.out.println(result);
    }
    
    static Integer distToAll(Pair<Integer, Integer> currCoordinate, Set<Pair<Integer, Integer>> elemList)
    {
        int runningSum = 0;
        
        for (Pair<Integer, Integer> elem : elemList)
        {
            runningSum += Math.abs(elem.getKey() - currCoordinate.getKey()) +
                          Math.abs(elem.getValue() - currCoordinate.getValue());
        }
        
        return runningSum;
    }
}
