import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.ZoneId;

public class AoC {
    public static void main(String[] args) {
        for (int i = 1; i <= 25; i++)
            for (int p = 1; p <= 2; p++)
            {
                String cName = "day" + i + ".D" + i + "P" + p;

                try {
                    Method main = Class.forName(cName).getMethod("main", String[].class);

                    System.out.println("Day " + i + " part " + p);
                    main.invoke(null, new Object[]{args});

                    System.out.println("\n");
                } catch (ClassNotFoundException e) {
                    LocalDate date = LocalDate.now(ZoneId.of("EST", ZoneId.SHORT_IDS));
                    LocalDate unlockTime = LocalDate.parse("2018-12-" + i);

                    if (!date.isBefore(unlockTime))
                        System.out.println("No class found for day " + i + " part " + p + " even though challenge is available");
                } catch (NoSuchMethodException e) {
                    System.out.println("No main method for day " + i + " part " + p);
                } catch (IllegalAccessException e) {
                    System.out.println("Main can't be accessed for day " + i + " part " + p);
                } catch (InvocationTargetException e) {
                    System.out.println("Wrong main invocation target for day " + i + " part " + p);
                }
            }

    }
}
