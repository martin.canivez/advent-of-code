package day19;

import day19.D19Util.*;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static day19.D19Util.*;

public class D19P1
{
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day19/input.txt")));
        List<Instruction> insList = new ArrayList<>();
        int ipReg = buildInsList(data, insList);
        int reg[] = new int[6];
        
        while (reg[ipReg] >= 0 && reg[ipReg] < insList.size())
        {
            Instruction i = insList.get(reg[ipReg]);
            reg = i.op.operate(reg, i.p1, i.p2, i.out);
            reg[ipReg]++;
        }
    
        System.out.println("Register 0 : " + reg[0]);
    }
}
