package day19;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class D19Util
{
    static List<Operation> opList = Arrays.asList(
            new Operation("addr", (reg, src1, src2) -> reg[src1] + reg[src2]),
            new Operation("addi", (reg, src1, src2) -> reg[src1] + src2),
            
            new Operation("mulr", (reg, src1, src2) -> reg[src1] * reg[src2]),
            new Operation("muli", (reg, src1, src2) -> reg[src1] * src2),
            
            new Operation("banr", (reg, src1, src2) -> reg[src1] & reg[src2]),
            new Operation("bani", (reg, src1, src2) -> reg[src1] & src2),
            
            new Operation("borr", (reg, src1, src2) -> reg[src1] | reg[src2]),
            new Operation("bori", (reg, src1, src2) -> reg[src1] | src2),
            
            new Operation("setr", (reg, src1, src2) -> reg[src1]),
            new Operation("seti", (reg, src1, src2) -> src1),
            
            new Operation("gtir", (reg, src1, src2) -> src1 > reg[src2] ? 1 : 0),
            new Operation("gtri", (reg, src1, src2) -> reg[src1] > src2 ? 1 : 0),
            new Operation("gtrr", (reg, src1, src2) -> reg[src1] > reg[src2] ? 1 : 0),
            
            new Operation("eqir", (reg, src1, src2) -> src1 == reg[src2] ? 1 : 0),
            new Operation("eqri", (reg, src1, src2) -> reg[src1] == src2 ? 1 : 0),
            new Operation("eqrr", (reg, src1, src2) -> reg[src1] == reg[src2] ? 1 : 0)
    );
    
    static class Instruction
    {
        public Operation op;
        public int p1;
        public int p2;
        public int out;
    
        public Instruction(Operation op, int p1, int p2, int out)
        {
            this.op = op;
            this.p1 = p1;
            this.p2 = p2;
            this.out = out;
        }
    
        public Instruction(String opName, int p1, int p2, int out)
        {
            this(opList.stream().filter(o -> o.opName.equals(opName)).findFirst().orElseThrow(RuntimeException::new), p1, p2, out);
        }
    
        @Override
        public String toString()
        {
            return "Instruction{" +
                   "op=" + op.opName +
                   ", p1=" + p1 +
                   ", p2=" + p2 +
                   ", out=" + out +
                   '}';
        }
    }
    
    static class Operation
    {
        public String opName;
        public OpFunction behavior;
        
        public Operation(String opName, OpFunction behavior) {
            this.opName = opName;
            this.behavior = behavior;
        }
        
        public int[] operate(int registers[], int src1, int src2, int out)
        {
            int outReg[] = Arrays.copyOf(registers, 6);
            int outVal = behavior.apply(registers, src1, src2);
            outReg[out] = outVal;
            return outReg;
        }
        
        @Override
        public String toString() {
            return "Operation{" +
                   "opName='" + opName + '\'' +
                   '}';
        }
    }
    
    @FunctionalInterface
    interface OpFunction {
        int apply(int reg[], int src1, int src2);
    }
    
    static int buildInsList(List<String> data, List<Instruction> insList)
    {
        Pattern extractor = Pattern.compile("([a-z]+) (\\d+) (\\d+) (\\d+)");
        int     ipReg = -1;
    
        for (String elem : data)
        {
            if (elem.startsWith("#ip"))
            {
                ipReg = Integer.parseInt(elem.split(" ")[1]);
                continue;
            }
        
            Matcher m = extractor.matcher(elem);
            m.matches();
        
            insList.add(new D19Util.Instruction(m.group(1), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4))));
        }
        
        return ipReg;
    }
    
    static int biggestIndex(int reg[])
    {
        int cmpReg = 0;
    
        for (int i = 0; i < reg.length; i++) {
            cmpReg = reg[i] > reg[cmpReg] ? i : cmpReg;
        }
        
        return cmpReg;
    }
}
