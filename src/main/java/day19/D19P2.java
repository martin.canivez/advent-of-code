package day19;

import day19.D19Util.*;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static day19.D19Util.*;

public class D19P2
{
    //The last part of the program comes from studying the input with a notepad and extracting the role of each register,
    //and extracting the loops of the program. From there I deducted the logic of the program, implemented below
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day19/input.txt")));
        List<Instruction> insList = new ArrayList<>();
        int ipReg = buildInsList(data, insList);
        int reg[] = new int[6];
        reg[0] = 1;
        
        while (reg[ipReg] != 1)
        {
            Instruction i = insList.get(reg[ipReg]);
            reg = i.op.operate(reg, i.p1, i.p2, i.out);
            reg[ipReg]++;
        }
    
        int cmpReg = biggestIndex(reg);
        long out = 0;
        
        for (int reg1F = 1; reg1F <= Math.sqrt(reg[cmpReg]); reg1F++)
        {
            if (reg[cmpReg]%reg1F == 0)
                out += reg1F + (reg[cmpReg]/reg1F);
        }
        
        System.out.println("Register 0 : " + out);
    }
}
