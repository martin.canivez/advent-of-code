package day2;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class D2P1
{
    public static void main(String[] args) throws IOException {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day2/input.txt")));

        int repeated2 = 0;
        int repeated3 = 0;

        for(String s : data)
        {
            Map<Character, Integer> letters = new HashMap<>();

            for (char c : s.toCharArray())
                letters.put(c, letters.getOrDefault(c, 0)+1);

            if (letters.containsValue(2))
                repeated2++;

            if (letters.containsValue(3))
                repeated3++;
        }

        System.out.println(repeated2 + " * " + repeated3 + " = " + repeated2*repeated3);
    }
}
