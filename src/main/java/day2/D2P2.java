package day2;

import javafx.util.Pair;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D2P2
{
    public static void main(String[] args) throws IOException {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day2/input.txt")));

        Set<Pair<String, String>> ids = new HashSet<>();

        for (int i = 0; i < data.size()-1; i++)
            for (int j = i+1; j < data.size(); j++)
            {
                ids.add(new Pair<>(data.get(i), data.get(j)));
            }

        for (Pair<String, String> currId : ids)
        {
            int diffChar = findDiff(currId);
            if (diffChar != -1)
            {
                System.out.println(currId.getKey() + "\n" +
                                   currId.getValue() + " : " + new StringBuilder(currId.getKey()).deleteCharAt(diffChar));
                return;
            }
        }
    }

    private static int findDiff(Pair<String, String> toTest)
    {
        String s1 = toTest.getKey(), s2 = toTest.getValue();
        int diffChar = -1;

        for (int i = 0; i < s1.length(); i++)
        {
            if (s1.charAt(i) != s2.charAt(i))
            {
                if (diffChar != -1)
                    return -1;

                diffChar = i;
            }
        }

        return diffChar;
    }
}
