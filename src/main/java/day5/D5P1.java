package day5;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Martin
 * created on 05/12/2018.
 */
public class D5P1
{
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day5/input.txt")));
        String polymer = D5Util.reducePolymer(data.get(0));
    
        System.out.println(polymer.length());
    }
}
