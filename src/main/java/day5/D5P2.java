package day5;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Martin
 * created on 05/12/2018.
 */
public class D5P2
{
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day5/input.txt")));
        String motherPolymer = D5Util.reducePolymer(data.get(0));
        
        char bestC = 0;
        int bestL = Integer.MAX_VALUE;
    
        for (char c = 'a', mc = 'A'; c <= 'z'; c++, mc++)
        {
            String polymer = motherPolymer.replace("" + c, "")
                                          .replace("" + mc, "");
            
            polymer = D5Util.reducePolymer(polymer);
    
            if(polymer.length() < bestL)
            {
                bestL = polymer.length();
                bestC = c;
            }
        }
    
        System.out.println("[" + bestC + " : " + bestL + "]");
    }
}
