package day5;

import java.util.Stack;

/**
 * Author: Martin
 * created on 05/12/2018.
 */
class D5Util
{
    // Optimized thanks to Julien2313 (https://github.com/Julien2313/adventOfCode)
    // and gregtwice (https://github.com/gregtwice/AOC2018)
    static String reducePolymer(String polymer)
    {
        Stack<Character> pb = new Stack<>();
        
        for (char c : polymer.toCharArray())
            if (pb.size() == 0)
                pb.push(c);
            else if (Math.abs(c - pb.peek()) == 'a'-'A')
                pb.pop();
            else
                pb.push(c);
        
        StringBuilder sb = new StringBuilder();
        pb.forEach(sb::append);
        
        return sb.reverse().toString();
    }
}
