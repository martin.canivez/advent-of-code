package day10;

public class D10Util
{
    static class Beam {
        public Beam(int x, int y, int vx, int vy)
        {
            this.x = x;
            this.y = y;
            this.vx = vx;
            this.vy = vy;
        }

        public void advance(int secs)
        {
            x += vx*secs;
            y += vy*secs;
        }

        public int x;
        public int y;
        public int vx;
        public int vy;

        @Override
        public String toString() {
            return "Beam{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }
}
