package day10;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class D10P1
{
    public static void main(String[] args) throws IOException {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day10/input.txt")));
        List<D10Util.Beam> beams = new ArrayList<>();

        for (String elem : data)
        {
            List<String> ls = Arrays.asList(elem.replaceAll("[^-?0-9]+", " ").trim().split(" "));
            List<Integer> li = ls.stream().map(Integer::parseInt).collect(Collectors.toList());

            beams.add(new D10Util.Beam(li.get(0), li.get(1), li.get(2), li.get(3)));
        }
        
        int minY = beams.stream().map(e -> e.y).min(Integer::compareTo).orElseThrow(RuntimeException::new);
        int maxY = beams.stream().map(e -> e.y).max(Integer::compareTo).orElseThrow(RuntimeException::new);
        int lastMinY = minY-1;
        int lastMaxY = maxY+1;
        int i = 0;

        while ((lastMaxY - lastMinY) > (maxY - minY))
        {
            beams.forEach(b -> b.advance(1));
            
            lastMinY = minY;
            lastMaxY = maxY;
            
            minY = beams.stream().map(e -> e.y).min(Integer::compareTo).orElseThrow(RuntimeException::new);
            maxY = beams.stream().map(e -> e.y).max(Integer::compareTo).orElseThrow(RuntimeException::new);
            i++;
        }
        
        beams.forEach(b -> b.advance(-1));
    
        int minX = beams.stream().map(e -> e.x).min(Integer::compareTo).orElseThrow(RuntimeException::new);
        int maxX = beams.stream().map(e -> e.x).max(Integer::compareTo).orElseThrow(RuntimeException::new);

        beams.sort((b1, b2) -> b1.y != b2.y ? Integer.compare(b1.y, b2.y) : Integer.compare(b1.x, b2.x));

        int lastX = minX;
        int lastY = lastMinY;

        for (D10Util.Beam elem : beams) {
            for (; lastY < elem.y; lastY++) {
                for (; lastX <= maxX; lastX++)
                    System.out.print(" ");

                lastX = minX;
                System.out.println();
            }

            for (; lastX < elem.x; lastX++) {
                System.out.print(" ");
            }

            if (elem.x == lastX) {
                System.out.print("#");
                lastX++;
            }
        }

        System.out.println("\n");
        System.out.println(i + " iterations");
    }
}
