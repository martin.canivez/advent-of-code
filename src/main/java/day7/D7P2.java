package day7;

import javafx.util.Pair;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Author: Martin
 * created on 07/12/2018.
 */
public class D7P2
{
    public static void main(String[] args) throws IOException
    {
        List<String> data      = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day7/input_help.txt")));
        Map<Character, List<Character>> tasks = D7Util.buildTasks(data);
    
        List<Pair<Character, List<Character>>> taskL = generateList(tasks);
        
        Work[] workers = new Work[5];
        List<Character> done = new ArrayList<>();
        int currTime = 0;
        
        while (done.size() < 26)
        {
            for (int i = 0; i < workers.length; i++)
            {
                if (workers[i] != null && workers[i].getEnd() <= currTime)
                {
                    done.add(workers[i].getTask());
                    workers[i] = chooseNextTask(taskL, done, currTime);
                    i = -1;
                }
                else if (workers[i] == null)
                    workers[i] = chooseNextTask(taskL, done, currTime);
            }
            
            if (done.size() < 26)
                currTime++;
        }
    
        System.out.println(currTime);
    }
    
    static List<Pair<Character, List<Character>>> generateList(Map<Character, List<Character>> tasks)
    {
        List<Pair<Character, List<Character>>> ret = new LinkedList<>();
    
        List<Pair<Character, List<Character>>> unordered = tasks.entrySet()
                                                                .stream()
                                                                .map(e -> new Pair<>(e.getKey(), e.getValue()))
                                                                .sorted(D7Util.comp)
                                                                .collect(Collectors.toList());
        
        while (unordered.size() > 0)
        {
            for (Pair<Character, List<Character>> elem : unordered)
            {
                if (checkDeps(elem, ret))
                    ret.add(elem);
            }
            
            unordered.removeAll(ret);
        }
        
        return ret;
    }
    
    static boolean checkDeps(Pair<Character, List<Character>> c, List<Pair<Character, List<Character>>> ref)
    {
        return checkAvailable(c, ref.stream().map(Pair::getKey).collect(Collectors.toList()));
    }
    
    static boolean checkAvailable(Pair<Character, List<Character>> c, List<Character> done)
    {
        for (Character currDep : c.getValue())
        {
            if (done.parallelStream().noneMatch(e -> e == currDep))
                return false;
        }
        
        return true;
    }
    
    static Work chooseNextTask(List<Pair<Character, List<Character>>> tList, List<Character> done, int timeStart)
    {
        Pair<Character, List<Character>> chosen = null;
        
        for (Pair<Character, List<Character>> elem : tList)
        {
            if (checkAvailable(elem, done))
            {
                chosen = elem;
                break;
            }
        }
        if (chosen != null)
        {
            tList.remove(chosen);
    
            return new Work(chosen.getKey(), timeStart, timeStart + 61 + (chosen.getKey() - 'A'));
        }
        else
            return null;
    }
    
    static class Work
    {
        public Work(char task, int start, int end)
        {
            this.task = task;
            this.start = start;
            this.end = end;
        }
    
        public char getTask()
        {
            return task;
        }
    
        public int getStart()
        {
            return start;
        }
    
        public int getEnd()
        {
            return end;
        }
    
        @Override
        public String toString()
        {
            return "Work{" +
                   "task=" + task +
                   ", start=" + start +
                   ", end=" + end +
                   '}';
        }
    
        private char task;
        private int  start;
        private int  end;
    }
}
