package day7;

import javafx.util.Pair;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: Martin
 * created on 07/12/2018.
 */
public class D7Util
{
    static Comparator<Pair<Character, List<Character>>> comp = (e1, e2) -> {
        if (e1.getValue().size() != e2.getValue().size())
            return Integer.compare(e1.getValue().size(), e2.getValue().size());
        else
            return Character.compare(e1.getKey(), e2.getKey());
    };
    
    static Map<Character, List<Character>> buildTasks(List<String> input)
    {
        Pattern                         extractor = Pattern.compile("Step ([A-Z]) must be finished before step ([A-Z]) can begin.");
        Map<Character, List<Character>> tasks     = new HashMap<>();
    
        for (String currData : input)
        {
            Matcher m = extractor.matcher(currData);
            m.matches();
        
            char currTask = m.group(2).charAt(0);
            char currDep = m.group(1).charAt(0);
            
            tasks.putIfAbsent(currTask, new ArrayList<>());
            tasks.putIfAbsent(currDep, new ArrayList<>());
        
            tasks.get(currTask).add(currDep);
        }
        
        return tasks;
    }
}
