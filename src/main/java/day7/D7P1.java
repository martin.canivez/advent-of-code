package day7;

import javafx.util.Pair;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Author: Martin
 * created on 07/12/2018.
 */
public class D7P1
{
    
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day7/input.txt")));
        Map<Character, List<Character>> tasks = D7Util.buildTasks(data);
        
        List<Pair<Character, List<Character>>> tList = tasks.entrySet()
                                                            .stream()
                                                            .map(e -> new Pair<>(e.getKey(), e.getValue()))
                                                            .sorted(D7Util.comp)
                                                            .collect(Collectors.toList());
        
        while (tList.size() > 0)
        {
            Character currChar = tList.get(0).getKey();
    
            System.out.print(currChar);
            
            tList.remove(0);
            
            for (Pair<Character, List<Character>> tElem : tList)
                tElem.getValue().remove(currChar);
    
            tList.sort(D7Util.comp);
        }
    
        System.out.println();
    }
}
