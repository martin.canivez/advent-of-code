package day12;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class D12Util {
    static boolean stabilized(Map<Long, Boolean> p1, Map<Long, Boolean> p2)
    {
        long minP1 = p1.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).min(Long::compareTo).orElseThrow(RuntimeException::new);
        long maxP1 = p1.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).max(Long::compareTo).orElseThrow(RuntimeException::new);
        long minP2 = p2.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).min(Long::compareTo).orElseThrow(RuntimeException::new);
        long maxP2 = p2.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).max(Long::compareTo).orElseThrow(RuntimeException::new);

        if (maxP1-minP1 != maxP2-minP2)
            return false;

        for (long i = minP1, j = minP2; i < maxP1 && j < maxP2; i++, j++)
        {
            if (p1.containsKey(i) ^ p2.containsKey(j))
                return false;
            
            if (p1.get(i) != p2.get(j))
                return false;
        }

        return true;
    }
    
    static Map<Long, Boolean> buildPots(String startString)
    {
        Map<Long, Boolean> plants = new HashMap<>();
        
        for (int i = 0; i < startString.length(); i++)
            plants.put((long)i, startString.charAt(i) == '#');
        
        return plants;
    }
    
    static Map<List<Boolean>, Boolean> buildEvo(List<String> evoString)
    {
        Map<List<Boolean>, Boolean> evo = new HashMap<>();
    
        for (String elem : evoString)
        {
            String e[] = elem.split(" => ");
        
            evo.put(e[0].chars().mapToObj(c -> (char) c).map(c -> c == '#').collect(Collectors.toList()), e[1].charAt(0) == '#');
        }
        
        return evo;
    }
    
    static void newEpoch(Map<Long, Boolean> plants, Map<List<Boolean>, Boolean> evo)
    {
        Map<Long, Boolean> newPlants = new HashMap<>();
    
        long min = plants.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).min(Long::compareTo).orElseThrow(RuntimeException::new);
        long max = plants.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).max(Long::compareTo).orElseThrow(RuntimeException::new);
    
        for (long currPot = min - 2; currPot <= max + 2; currPot++)
        {
            List<Boolean> spot = new ArrayList<>();
            for (long pos = currPot - 2; pos <= currPot + 2; pos++)
                spot.add(plants.getOrDefault(pos, false));
        
            newPlants.put(currPot, evo.getOrDefault(spot, false));
        }
    
        plants.clear();
        plants.putAll(newPlants);
    }
}
