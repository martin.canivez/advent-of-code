package day12;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static day12.D12Util.*;

public class D12P1
{
    public static void main(String[] args) throws IOException {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day12/input.txt")));
    
        Map<Long, Boolean> plants = buildPots(data.get(0).split(" ")[2]);
    
        Map<List<Boolean>, Boolean> evo = buildEvo(data.subList(2, data.size()));
    
        for (long gen = 1; gen <= 20; gen++)
        {
                newEpoch(plants, evo);
        }
    
        System.out.println(plants.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).reduce(0L, Long::sum));
    }
}
