package day12;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import static day12.D12Util.*;

public class D12P2
{
    public static void main(String[] args) throws IOException {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day12/input.txt")));
    
        Map<Long, Boolean> plants = buildPots(data.get(0).split(" ")[2]);
    
        Map<List<Boolean>, Boolean> evo = buildEvo(data.subList(2, data.size()));

        long lastScore = 0;
        long lastScoreDiff = 0;
        boolean isStab = false;

        for (long gen = 1; gen <= 50000000000L; gen++)
        {
            if (!isStab) {
                Map<Long, Boolean> oldPlants = new HashMap<>(plants);

                newEpoch(plants, evo);

                long newScore = plants.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).reduce(0L, Long::sum);
                long scoreDiff = newScore - lastScore;

                isStab = stabilized(oldPlants, plants);

                lastScore = newScore;
                lastScoreDiff = scoreDiff;
            }
            else
            {
                long diffToFinalGen = 50000000001L - gen;
                lastScore += lastScoreDiff*diffToFinalGen;
                gen += diffToFinalGen;
            }
        }

        System.out.println(lastScore);
    }
}
