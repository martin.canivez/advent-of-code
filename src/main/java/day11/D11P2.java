package day11;

import java.util.Arrays;

import static day11.D11Util.computeTotal;

public class D11P2 {
    public static void main(String[] args) {
        int input = 5034;

        Integer[][] cell = new Integer[300][300];

        for (int i = 1; i <= 300; i++)
        {
            for (int j = 1; j <= 300; j++)
            {
                int val = D11Util.computeEnergy(i, j, input);

                cell[i-1][j-1] = val;
            }
        }

        int max = Integer.MIN_VALUE;
        int x = 0;
        int y = 0;
        int size = 0;
        
        Integer computeMap[][] = new Integer[300][300];
        
        for (Integer row[] : computeMap)
            Arrays.fill(row, Integer.MIN_VALUE);

        for (int currSize = 1; currSize <= 300; currSize++) {
            for (int i = 1; i <= 300 - currSize + 1; i++) {
                for (int j = 1; j <= 300 - currSize + 1; j++) {
                    int currEnergy = computeTotal(i, j, i+currSize-1, j+currSize-1, cell, computeMap);

                    if (currEnergy > max) {
                        max = currEnergy;
                        x = i;
                        y = j;
                        size = currSize;
                    }
                }
            }
        }

        System.out.println(x + "," + y + "," + size);
    }
}
