package day11;

public class D11Util {
    static int computeEnergy(int x, int y, int gridSerial)
    {
        int rackId = x+10;
        int currentPower = rackId*y;
        currentPower += gridSerial;
        currentPower *= rackId;
        currentPower = (currentPower/100)%10;
        currentPower -= 5;

        return currentPower;
    }
    
    static int computeTotal(int x1,
                            int y1,
                            int x2,
                            int y2,
                            Integer energyMap[][],
                            Integer computeMap[][])
    {
    
        if (x1 < 1 || x2 < 1 || y1 < 1 || y2 < 1)
            return 0;
        
        if (x1 == 1 && y1 == 1)
        {
            if (computeMap[x2-1][y2-1] != Integer.MIN_VALUE)
                return computeMap[x2-1][y2-1];
            
            int total =  computeTotal(1, 1, x2, y2-1, energyMap, computeMap) +
                         computeTotal(1, 1, x2-1, y2, energyMap, computeMap) -
                         computeTotal(1, 1, x2-1, y2-1, energyMap, computeMap) +
                         energyMap[x2-1][y2-1];
            
            computeMap[x2-1][y2-1] = total;
            
            return total;
        }
        else
        {
            return computeTotal(1, 1, x2, y2, energyMap, computeMap) +
                   computeTotal(1, 1, x1-1, y1-1, energyMap, computeMap) -
                   computeTotal(1, 1, x1-1, y2, energyMap, computeMap) -
                   computeTotal(1, 1, x2, y1-1, energyMap, computeMap);
        }
    }
}
