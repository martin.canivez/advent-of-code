package day11;

import java.util.ArrayList;
import java.util.List;

public class D11P1
{
    public static void main(String[] args) {
        int input = 5034;

        Integer[][] cell = new Integer[300][300];

        for (int i = 1; i <= 300; i++)
        {
            List<Integer> row = new ArrayList<>();

            for (int j = 1; j <= 300; j++)
            {
                int val = D11Util.computeEnergy(i, j, input);

                cell[i-1][j-1] = val;
            }
        }

        int max = Integer.MIN_VALUE;
        int x = 0;
        int y = 0;

        for (int i = 1; i <= 300-2; i++)
        {
            for (int j = 1; j <= 300-2; j++)
            {
                int currEnergy = 0;

                for (int a = 0; a < 3; a++)
                    for (int b = 0; b < 3; b++)
                        currEnergy += cell[i+a-1][j+b-1];

                if (currEnergy > max) {
                    max = currEnergy;
                    x = i;
                    y = j;
                }
            }
        }

        System.out.println(x + "," + y);
    }
}
