package day14;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class D14P1 {
    public static void main(String[] args) {
        int input = 380621;

        List<Integer> recipes = new ArrayList<>(Arrays.asList(3, 7));

        int e1Recipe = 0;
        int e2Recipe = 1;

        while (recipes.size() < input+10)
        {
            int nRecipe = recipes.get(e1Recipe)+recipes.get(e2Recipe);

            if (nRecipe > 9)
            {
                recipes.add(nRecipe/10);
                nRecipe %= 10;
            }

            recipes.add(nRecipe);

            e1Recipe = (e1Recipe + 1 + recipes.get(e1Recipe))%recipes.size();
            e2Recipe = (e2Recipe + 1 + recipes.get(e2Recipe))%recipes.size();
        }

        for (int i = input; i < input+10; i++)
            System.out.print(recipes.get(i));

        System.out.println();
    }
}
