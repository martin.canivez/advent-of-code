package day14;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class D14P2 {
    public static void main(String[] args) {
        int input = 380621;
        String inputString = Integer.toString(input);

        int lastIndex = -1;

        List<Integer> recipes = new ArrayList<>(Arrays.asList(3, 7));

        int e1Recipe = 0;
        int e2Recipe = 1;

        while (lastIndex++ > -2)
        {
            while (recipes.size() <= lastIndex+inputString.length()) {

                int nRecipe = recipes.get(e1Recipe) + recipes.get(e2Recipe);

                if (nRecipe > 9) {
                    recipes.add(nRecipe / 10);
                    nRecipe %= 10;
                }

                recipes.add(nRecipe);

                e1Recipe = (e1Recipe + 1 + recipes.get(e1Recipe)) % recipes.size();
                e2Recipe = (e2Recipe + 1 + recipes.get(e2Recipe)) % recipes.size();
            }

            StringBuilder currentTested = new StringBuilder();

            for (int i = lastIndex; i < lastIndex+inputString.length(); i++)
                currentTested.append(Integer.toString(recipes.get(i)));

            if (currentTested.toString().equals(inputString))
                break;
        }

        System.out.println(lastIndex);
    }
}
