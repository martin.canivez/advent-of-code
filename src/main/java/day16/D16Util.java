package day16;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class D16Util {
    static List<Operation> opList = Arrays.asList(
            new Operation("addr", (reg, src1, src2) -> reg[src1] + reg[src2]),
            new Operation("addi", (reg, src1, src2) -> reg[src1] + src2),

            new Operation("mulr", (reg, src1, src2) -> reg[src1] * reg[src2]),
            new Operation("muli", (reg, src1, src2) -> reg[src1] * src2),

            new Operation("banr", (reg, src1, src2) -> reg[src1] & reg[src2]),
            new Operation("bani", (reg, src1, src2) -> reg[src1] & src2),

            new Operation("borr", (reg, src1, src2) -> reg[src1] | reg[src2]),
            new Operation("bori", (reg, src1, src2) -> reg[src1] | src2),

            new Operation("setr", (reg, src1, src2) -> reg[src1]),
            new Operation("seti", (reg, src1, src2) -> src1),

            new Operation("gtir", (reg, src1, src2) -> src1 > reg[src2] ? 1 : 0),
            new Operation("gtri", (reg, src1, src2) -> reg[src1] > src2 ? 1 : 0),
            new Operation("gtrr", (reg, src1, src2) -> reg[src1] > reg[src2] ? 1 : 0),

            new Operation("eqir", (reg, src1, src2) -> src1 == reg[src2] ? 1 : 0),
            new Operation("eqri", (reg, src1, src2) -> reg[src1] == src2 ? 1 : 0),
            new Operation("eqrr", (reg, src1, src2) -> reg[src1] == reg[src2] ? 1 : 0)
    );

    static List<Operation> allFitting(Observation o)
    {
        return opList.stream().filter(op -> Arrays.equals(op.operate(o.before, o.src1, o.src2, o.outReg), o.after)).collect(Collectors.toList());
    }

    static void removeAndCheck(List<Operation> op, Collection<List<Operation>> list)
    {
        for (List<Operation> e : list)
        {
            if (e == op)
                continue;

            int sizeB = e.size();
            e.remove(op.get(0));

            if (e.size() < sizeB && e.size() == 1)
                removeAndCheck(e, list);
        }
    }

    static class Observation
    {
        public int before[] = new int[4];
        public int opCode;
        public int src1;
        public int src2;
        public int outReg;
        public int after[];

        public Observation(int[] before, int opCode, int src1, int src2, int outReg, int[] after) {
            this.before = before;
            this.opCode = opCode;
            this.src1 = src1;
            this.src2 = src2;
            this.outReg = outReg;
            this.after = after;
        }
    }

    static class Operation
    {
        public String opName;
        public OpFunction behavior;

        public Operation(String opName, OpFunction behavior) {
            this.opName = opName;
            this.behavior = behavior;
        }

        public int[] operate(int registers[], int src1, int src2, int out)
        {
            int outReg[] = Arrays.copyOf(registers, 4);
            int outVal = behavior.apply(registers, src1, src2);
            outReg[out] = outVal;
            return outReg;
        }

        @Override
        public String toString() {
            return "Operation{" +
                    "opName='" + opName + '\'' +
                    '}';
        }
    }

    @FunctionalInterface
    interface OpFunction {
        public int apply(int reg[], int src1, int src2);
    }
}
