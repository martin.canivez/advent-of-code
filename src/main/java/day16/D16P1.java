package day16;

import day16.D16Util.*;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static day16.D16Util.*;

public class D16P1 {
    public static void main(String[] args) throws IOException {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day16/input.txt")));
        Pattern beforePattern = Pattern.compile("Before: \\[(\\d+), (\\d+), (\\d+), (\\d+)]");
        Pattern afterPattern =  Pattern.compile("After:  \\[(\\d+), (\\d+), (\\d+), (\\d+)]");

        List<Observation> allObs = new ArrayList<>();

        int counter = 0;

        while (true)
        {
            if (data.size() < 1 || data.get(0).equals("") && data.get(1).equals(""))
                break;

            String beforeStr = data.remove(0);
            String opStr[] = data.remove(0).split(" ");
            String afterStr = data.remove(0);

            if (data.size() > 0 && data.get(0).equals(""))
                data.remove(0);

            Matcher mBefore = beforePattern.matcher(beforeStr);
            Matcher mAfter = afterPattern.matcher(afterStr);
            mBefore.matches();
            mAfter.matches();

            int before[] = {Integer.parseInt(mBefore.group(1)),
                            Integer.parseInt(mBefore.group(2)),
                            Integer.parseInt(mBefore.group(3)),
                            Integer.parseInt(mBefore.group(4))};

            int after[] = {Integer.parseInt(mAfter.group(1)),
                           Integer.parseInt(mAfter.group(2)),
                           Integer.parseInt(mAfter.group(3)),
                           Integer.parseInt(mAfter.group(4))};

            allObs.add(new Observation(before, Integer.parseInt(opStr[0]), Integer.parseInt(opStr[1]), Integer.parseInt(opStr[2]), Integer.parseInt(opStr[3]), after));
        }

        for (Observation currObs : allObs)
        {
            if (allFitting(currObs).size() >= 3)
                counter++;
        }

        System.out.println(counter);
    }
}
