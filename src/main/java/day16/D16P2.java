package day16;

import day16.D16Util.*;
import javafx.util.Pair;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static day16.D16Util.*;

public class D16P2 {

    public static void main(String[] args) throws IOException {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day16/input.txt")));
        Pattern beforePattern = Pattern.compile("Before: \\[(\\d+), (\\d+), (\\d+), (\\d+)]");
        Pattern afterPattern =  Pattern.compile("After:  \\[(\\d+), (\\d+), (\\d+), (\\d+)]");

        List<D16Util.Observation> allObs = new ArrayList<>();

        List<Integer[]> program = new ArrayList<>();

        while (true)
        {
            if (data.size() < 1 || data.get(0).equals("") && data.get(1).equals(""))
                break;

            String beforeStr = data.remove(0);
            String opStr[] = data.remove(0).split(" ");
            String afterStr = data.remove(0);

            if (data.size() > 0 && data.get(0).equals(""))
                data.remove(0);

            Matcher mBefore = beforePattern.matcher(beforeStr);
            Matcher mAfter = afterPattern.matcher(afterStr);
            mBefore.matches();
            mAfter.matches();

            int before[] = {Integer.parseInt(mBefore.group(1)),
                    Integer.parseInt(mBefore.group(2)),
                    Integer.parseInt(mBefore.group(3)),
                    Integer.parseInt(mBefore.group(4))};

            int after[] = {Integer.parseInt(mAfter.group(1)),
                    Integer.parseInt(mAfter.group(2)),
                    Integer.parseInt(mAfter.group(3)),
                    Integer.parseInt(mAfter.group(4))};

            allObs.add(new D16Util.Observation(before, Integer.parseInt(opStr[0]), Integer.parseInt(opStr[1]), Integer.parseInt(opStr[2]), Integer.parseInt(opStr[3]), after));
        }

        while (data.size() > 0)
        {
            if (data.get(0).equals("")) {
                data.remove(0);
                continue;
            }

            String opStr[] = data.remove(0).split(" ");
            program.add(new Integer[]{Integer.parseInt(opStr[0]), Integer.parseInt(opStr[1]), Integer.parseInt(opStr[2]), Integer.parseInt(opStr[3])});
        }

        Map<Integer, List<D16Util.Operation>> possibleOperations = new HashMap<>();
        for (int i = 0; i <= 15; i++)
            possibleOperations.put(i, new ArrayList<>(opList));

        while (possibleOperations.values().stream().anyMatch(lo -> lo.size() > 1))
        {
            Observation currObs = allObs.remove(0);

            List<Operation> currentFitting = possibleOperations.get(currObs.opCode);

            if (currentFitting.size() == 1)
                continue;

            List<Operation> fitting = allFitting(currObs);

            currentFitting.removeIf(pop -> !fitting.contains(pop));

            if (currentFitting.size() == 1)
                removeAndCheck(currentFitting, possibleOperations.values());
        }

        Map<Integer, Operation> correspondance = possibleOperations.entrySet()
                .stream()
                .map(e -> new Pair<>(e.getKey(), e.getValue().get(0))).collect(Collectors.toMap(Pair::getKey, Pair::getValue));

        int reg[] = {0, 0, 0, 0};

        for (Integer[] currOp : program)
        {
            reg = correspondance.get(currOp[0]).operate(reg, currOp[1], currOp[2], currOp[3]);
        }

        System.out.println(reg[0]);
    }
}
