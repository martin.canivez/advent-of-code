package day4;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Author: Martin
 * created on 04/12/2018.
 */
public class D4P2
{
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day4/input.txt")));
        
        Map<Integer, List<Integer>> sleepMap = D4Util.buildSleepMap(data);
        
        List<Map.Entry<Integer, List<Integer>>> l = sleepMap.entrySet()
                                                            .stream()
                                                            .sorted((d1, d2) -> -Collections.max(d1.getValue())
                                                                                            .compareTo(Collections.max(d2.getValue())))
                                                            .collect(Collectors.toList());
    
        Map.Entry<Integer, List<Integer>> first = l.get(0);
    
        System.out.println("Guard " +
                           first.getKey() +
                           " was asleep " +
                           first.getValue().stream().reduce(0, Integer::sum) +
                           " minutes");
    
        int biggest = 0;
    
        for (int i = 0; i < 60; i++)
            if (first.getValue().get(i) > first.getValue().get(biggest))
                biggest = i;
    
        System.out.println("Guard " +
                           first.getKey() +
                           " was asleep most often at 00:" +
                           biggest +
                           " (" +
                           first.getValue().get(biggest) +
                           " times overall)");
    
        System.out.println("Total : " + (first.getKey() * biggest));
    }
}
