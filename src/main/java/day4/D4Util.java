package day4;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Author: Martin
 * created on 04/12/2018.
 */
class D4Util
{
    static Map<Integer, List<Integer>> buildSleepMap(List<String> data)
    {
        Pattern extractor = Pattern.compile("\\[(([^\\[ ]*) (00|23):([0-9]+))] (falls asleep|wakes up|Guard #([0-9]+) begins shift)");
    
        Map<Integer, List<Integer>> sleepMap = new HashMap<>();
    
        List<String> sorted = data.stream().sorted().collect(Collectors.toList());
    
        int currentGuard = 0;
    
        for (String currDat : sorted)
        {
            Matcher m = extractor.matcher(currDat);
            m.matches();
        
            switch (m.group(5))
            {
            case "falls asleep":
                int toSleepMinute = Integer.parseInt(m.group(4));
                List<Integer> list1 = sleepMap.get(currentGuard);
            
                for (int i = toSleepMinute; i < 60; i++)
                    list1.set(i, list1.get(i)+1);
                
                break;
            case "wakes up":
                int toAwakeMinute = Integer.parseInt(m.group(4));
                List<Integer> list2 = sleepMap.get(currentGuard);
            
                for (int i = toAwakeMinute; i < 60; i++)
                    list2.set(i, list2.get(i)-1);
                
                break;
            default:
                currentGuard = Integer.parseInt(m.group(6));
                sleepMap.putIfAbsent(currentGuard, new LinkedList<>(Collections.nCopies(60, 0)));
            }
        }
        
        return sleepMap;
    }
}
