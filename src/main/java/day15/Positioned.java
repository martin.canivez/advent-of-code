package day15;

import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;
import org.apache.commons.math3.complex.Complex;

class Positioned implements Comparable<Positioned> {
    private Complex pos;

    public Positioned(int x, int y) {
        this.pos = new Complex(x, y);
    }

    public int x() {
        return (int)pos.getReal();
    }

    public void setX(int x) {
        pos = new Complex(x, pos.getImaginary());
    }

    public int y() {
        return (int)pos.getImaginary();
    }

    public void setY(int y) {
        pos = new Complex(pos.getReal(), y);
    }

    public Complex getPos() {
        return pos;
    }

    public void setPos(Complex pos)
    {
        this.pos = pos;
    }

    @Override
    public int compareTo(Positioned o) {
        return ComparisonChain.start()
                .compare(pos.getImaginary(), o.pos.getImaginary())
                .compare(pos.getReal(), o.pos.getReal())
                .result();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Positioned)) return false;
        Positioned that = (Positioned) o;
        return Objects.equal(pos, that.pos);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(pos);
    }

    @Override
    public String toString() {
        return "Positioned{" +
                "pos=" + pos +
                '}';
    }
}
