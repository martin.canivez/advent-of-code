package day15;

import com.google.common.collect.ComparisonChain;
import org.apache.commons.math3.complex.Complex;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Set;

public class D15Util {

    static final Set<Complex> moves = new LinkedHashSet<>(Arrays.asList(new Complex(0, -1),
                                                                        new Complex(-1, 0),
                                                                        new Complex(1, 0),
                                                                        new Complex(0, 1)));

    static int distanceBetween(int x1, int x2, int y1, int y2)
    {
        return Math.abs(x1-x2)+ Math.abs(y1-y2);
    }

    static Comparator<Complex> complexComparator = (c1, c2) -> ComparisonChain.start()
            .compare(c1.getImaginary(), c2.getImaginary())
            .compare(c1.getReal(), c2.getReal())
            .result();
}
