package day15;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D15P1 {
    public static void main(String[] args) throws IOException {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day15/input.txt")));

        Fight f = new Fight(data);
        int round = 0;
        boolean full = true;

        while (full) {
            full = f.doTurn();

            if (full)
                round++;
        }

        List<Fighter> w = f.elfs().size() > 0 ? f.elfs() : f.goblins();

        System.out.println(round + " * " + w.stream().map(Fighter::getLifePoints).reduce(0, Integer::sum));
        System.out.println(round * w.stream().map(Fighter::getLifePoints).reduce(0, Integer::sum));
    }
}
