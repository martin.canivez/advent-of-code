package day15;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D15P2 {
    public static void main(String[] args) throws IOException {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day15/input.txt")));

        int attackPower = 3;
        Fight f = new Fight(data, attackPower);
        int round = 0;
        boolean full = true;
        int elvesNumber = f.elfs().size();


        while (full) {
            while (full && elvesNumber == f.elfs().size()) {
                full = f.doTurn();

                if (full)
                    round++;
            }

            if (elvesNumber != f.elfs().size())
            {
                attackPower++;
                full = true;
                round = 0;
                f = new Fight(data, attackPower);
            }
        }

        List<Fighter> w = f.elfs().size() > 0 ? f.elfs() : f.goblins();

        System.out.println(round + " * " + w.stream().map(Fighter::getLifePoints).reduce(0, Integer::sum));
        System.out.println(round * w.stream().map(Fighter::getLifePoints).reduce(0, Integer::sum));
    }
}
