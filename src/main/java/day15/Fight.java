package day15;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.complex.Complex;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Fight {
    Set<Cell> terrain;

    private List<Fighter> m_elfs;
    private List<Fighter> m_goblins;

    public Fight(List<String> input)
    {
        this(input, 3);
    }

    public Fight(List<String> input, int elvesPower)
    {
        terrain = new TreeSet<>();
        m_goblins = new ArrayList<>();
        m_elfs = new ArrayList<>();

        for (int y = 0; y < input.size(); y++)
        {
            String row = input.get(y);

            for (int x = 0; x < row.length(); x++)
            {
                char c = row.charAt(x);
                switch (c)
                {
                    case '.':
                        terrain.add(new Cell(x, y, true));
                        break;
                    case '#':
                        terrain.add(new Cell(x, y, false));
                        break;
                    case 'G':
                        m_goblins.add(new Fighter(x, y));
                        terrain.add(new Cell(x, y, true));
                        break;
                    case 'E':
                        m_elfs.add(new Fighter(x, y, 200, elvesPower));
                        terrain.add(new Cell(x, y, true));
                        break;
                    default:
                        throw new RuntimeException(Character.toString(c));
                }
            }
        }
    }

    List<Fighter> elfs()
    {
        m_elfs.sort(Positioned::compareTo);
        return m_elfs;
    }

    List<Fighter> goblins()
    {
        m_goblins.sort(Positioned::compareTo);
        return m_goblins;
    }

    private List<Fighter> units()
    {
        return Stream.concat(elfs().stream(), goblins().stream()).sorted(Positioned::compareTo).collect(Collectors.toList());
    }

    private void cleanFighters()
    {
        m_elfs.removeIf(f -> !f.isAlive());
        m_goblins.removeIf(f -> !f.isAlive());
    }

    private void doTurn(Fighter f, List<Fighter> units, List<Fighter> enemies, Set<Cell> terrain)
    {
        //display();

        if (!f.isAlive())
            return;

        Fighter target = f.findTarget(enemies);

        if (target != null)
            f.attack(target);
        else
        {
            Cell next = Cell.getCell(terrain, f.getPos()).nextMove(terrain, enemies, units);

            if (next != null)
                f.setPos(next.getPos());

            target = f.findTarget(enemies);

            if (target != null)
                f.attack(target);
        }
    }

    public boolean doTurn()
    {
        cleanFighters();

        List<Fighter> units = units();

        for (Fighter unit : units)
        {
            if (elfs().stream().noneMatch(Fighter::isAlive) || goblins().stream().noneMatch(Fighter::isAlive)) {
                cleanFighters();
                return false;
            }

            doTurn(unit, units, elfs().contains(unit) ? goblins() : elfs(), terrain);
        }

        cleanFighters();

        return true;
    }

    public void display()
    {
        StringBuilder sb = new StringBuilder();

        int maxX = terrain.stream().map(Cell::getPos).map(Complex::getReal).max(Double::compareTo).orElseThrow(RuntimeException::new).intValue();
        int maxY = terrain.stream().map(Cell::getPos).map(Complex::getImaginary).max(Double::compareTo).orElseThrow(RuntimeException::new).intValue();
        int numberRow = (int) Math.ceil(Math.log10(maxX));
        int numberCol = (int) Math.ceil(Math.log10(maxY));



        for(double n=0; n < numberRow; n++)
        {
            sb.append(StringUtils.repeat(' ', numberCol+1));

            for (int i=0; i <= maxX; i++)
            {
                int number = (int) ((i/(Math.pow(10, numberRow-n-1)))%10);

                if (i < Math.pow(10, numberRow-n-1) && (i > 0 || n != numberRow-1))
                    sb.append(' ');
                else
                    sb.append(number);
            }

            sb.append('\n');
        }

        for (Cell c : terrain)
        {
            if (c.getPos().getReal() == 0)
            {
                int precY = (int) (c.getPos().getImaginary()-1);

                List<Fighter> f = units().stream().filter(u -> (int)(u.getPos().getImaginary()) == precY).collect(Collectors.toList());

                for (Fighter currF : f)
                {
                    sb.append(" ");
                    sb.append(elfs().contains(currF) ? "E(" : "G(").append(currF.lifePoints).append(')');

                }

                sb.append('\n');
                sb.append(StringUtils.leftPad(Integer.toString((int)c.getPos().getImaginary()), numberCol)).append(' ');
            }

            if (elfs().stream().anyMatch(e -> e.getPos().equals(c.getPos())))
                sb.append('E');
            else if (goblins().stream().anyMatch(e -> e.getPos().equals(c.getPos())))
                sb.append('G');
            else
                sb.append(c.isWalkable() ? ' ' : "#");
        }

        System.out.println(sb.toString());
    }
}
