package day15;

import javafx.util.Pair;
import org.apache.commons.math3.complex.Complex;

import java.util.*;

import static day15.D15Util.moves;

public class Cell extends Positioned {
    private boolean isWalkable;

    public Cell(int x, int y, boolean isWalkable) {
        super(x, y);
        this.isWalkable = isWalkable;
    }

    public boolean isWalkable() {
        return isWalkable;
    }

    Cell nextMove(Set<Cell> terrain, Collection<Fighter> enemies, Collection<Fighter> fighters)
    {
        LinkedList<Pair<Complex, Integer>> toVisit = new LinkedList<>();
        toVisit.add(new Pair<>(getPos(), 0));

        Map<Complex, Pair<Complex, Integer>> precMap = new HashMap<>();

        Set<Complex> seen = new HashSet<>();

        while(toVisit.size() > 0)
        {
            Pair<Complex, Integer> cell = toVisit.remove();

            for (Complex currMove : moves)
            {
                Complex next = cell.getKey().add(currMove);

                if (terrain.stream().noneMatch(tc -> tc.getPos().equals(next) && tc.isWalkable()))
                    continue;

                if (!precMap.containsKey(next) ||
                        precMap.get(next).getValue() > cell.getValue()+1 ||
                        (precMap.get(next).getValue().equals(cell.getValue()+1) && D15Util.complexComparator.compare(cell.getKey(), precMap.get(next).getKey()) < 0))
                    precMap.put(next, new Pair<>(cell.getKey(), cell.getValue()+1));

                if (seen.contains(next))
                    continue;

                if (fighters.stream().anyMatch(f -> f.getPos().equals(next) && f.isAlive()))
                    continue;

                if (toVisit.stream().noneMatch(tv -> tv.getKey().equals(next)))
                    toVisit.add(new Pair<>(next, cell.getValue()+1));

                seen.add(next);
            }
        }

        Map<Fighter, Pair<Complex, Integer>> distance = new HashMap<>();

        for (Fighter f : enemies)
        {
            if (!f.isAlive())
                continue;

            Pair<Complex, Integer> minDist = precMap.getOrDefault(f.getPos(), null);

            if (minDist != null)
                distance.put(f, minDist);
        }

        Map.Entry<Fighter, Pair<Complex, Integer>> closest = distance.entrySet().stream().sorted((e1, e2) -> D15Util.complexComparator.compare(e1.getValue().getKey(), e2.getValue().getKey())).min(Comparator.comparingInt(e -> e.getValue().getValue())).orElse(null);

        if (closest == null)
            return null;

        Pair<Complex, Integer> prevCell = closest.getValue();

        while (prevCell.getValue() > 2)
            prevCell = precMap.get(prevCell.getKey());

        return getCell(terrain, prevCell.getKey());
    }

    static Cell getCell(Set<Cell> terrain, Complex pos)
    {
        return terrain.stream().filter(t -> t.getPos().equals(pos)).findFirst().orElseThrow(RuntimeException::new);
    }

    @Override
    public String toString() {
        return "Cell{" +
                "isWalkable=" + isWalkable +
                "} " + super.toString();
    }
}
