package day15;

import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;

import java.util.List;

import static day15.D15Util.distanceBetween;
import static day15.D15Util.moves;

public class Fighter extends Positioned {
    int lifePoints;
    int force;

    public Fighter(int x, int y, int lifePoints, int force) {
        super(x, y);
        this.lifePoints = lifePoints;
        this.force = force;
    }

    public Fighter(int x, int y) {
        this(x, y, 200, 3);
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public int getForce() {
        return force;
    }

    public boolean reduceLife(int amount)
    {
        lifePoints -= amount;
        return isAlive();
    }

    public boolean isAlive()
    {
        return lifePoints > 0;
    }

    public boolean attack(Fighter target)
    {
        if (distanceBetween(x(), target.x(), y(), target.y()) > 1)
            throw new IllegalArgumentException("(" + x() + "," + y() + ")(" + target.x() + "," + target.y() + ")");

        return target.reduceLife(force);
    }

    public Fighter findTarget(List<Fighter> targets)
    {
        Fighter target = moves.stream()
                .map(m -> getPos().add(m))
                .map(p -> targets.stream()
                        .filter(e -> e.getPos().equals(p) && e.isAlive())
                        .findFirst()
                        .orElse(null))
                .filter(java.util.Objects::nonNull)
                .min((e1, e2) -> ComparisonChain.start().compare(e1.lifePoints, e2.lifePoints).compare(e1, e2).result())
                .orElse(null);

        return target;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Fighter)) return false;
        if (!super.equals(o)) return false;
        Fighter fighter = (Fighter) o;
        return lifePoints == fighter.lifePoints &&
                force == fighter.force;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), lifePoints, force);
    }

    @Override
    public String toString() {
        return "Fighter{" +
                "lifePoints=" + lifePoints +
                ", force=" + force +
                "} " + super.toString();
    }
}
