package day17;

import com.google.common.collect.Table;
import javafx.util.Pair;

import java.util.*;
import java.util.function.Supplier;

public class D17Util {
    enum Status {
        SAND,
        CLAY,
        WATER_STILL,
        WATER_FLOW,
    }
    
    static void setChange(Pair<Integer, Integer> p, Status nVal, Table<Integer, Integer, Status> table, Stack<Pair<Integer, Integer>> s, int maxY)
    {
        if (p.getValue() > maxY || p.getValue() < 1)
            return;
        if (table.contains(p.getKey(), p.getValue()) && table.get(p.getKey(), p.getValue()) == Status.CLAY)
            return;
        
        if (!table.contains(p.getKey(), p.getValue()) || table.get(p.getKey(), p.getValue()) != nVal)
        {
            table.put(p.getKey(), p.getValue(), nVal);
            s.push(new Pair<>(p.getKey(), p.getValue()+1));
            s.push(new Pair<>(p.getKey()+1, p.getValue()));
            s.push(new Pair<>(p.getKey()-1, p.getValue()));
            s.push(new Pair<>(p.getKey(), p.getValue()-1));
            s.push(p);
        }
    }
    
    static void process(Table<Integer, Integer, Status> table) throws InterruptedException
    {
        int maxY = table.columnKeySet().stream().max(Integer::compareTo).orElseThrow(RuntimeException::new);
        
        Stack<Pair<Integer, Integer>> s = new Stack<>();
        table.put(500, 0, Status.WATER_FLOW);
        s.add(new Pair<>(500, 0));
        
        while (s.size() > 0)
        {
            Pair<Integer, Integer> e = s.pop();
            
            Supplier<Status> cStat = () -> table.get(e.getKey(), e.getValue());
            Supplier<Status> below = () -> table.get(e.getKey(), e.getValue()+1);
            
            if (cStat.get() == Status.CLAY)
                continue;
            
            if (cStat.get() == Status.WATER_FLOW && table.get(e.getKey(), e.getValue()+1) == Status.SAND)
                setChange(new Pair<>(e.getKey(), e.getValue()+1), Status.WATER_FLOW, table, s, maxY);
            
            if (cStat.get() == Status.WATER_FLOW && isSupported(e, table))
                setChange(e, Status.WATER_STILL, table, s, maxY);
            
            if (cStat.get() == Status.WATER_FLOW && (below.get() == Status.CLAY || below.get() == Status.WATER_STILL))
            {
                setChange(new Pair<>(e.getKey()-1, e.getValue()), Status.WATER_FLOW, table, s, maxY);
                setChange(new Pair<>(e.getKey()+1, e.getValue()), Status.WATER_FLOW, table, s, maxY);
            }
            
            if (cStat.get() == Status.WATER_STILL)
            {
                setChange(new Pair<>(e.getKey()-1, e.getValue()), Status.WATER_STILL, table, s, maxY);
                setChange(new Pair<>(e.getKey()+1, e.getValue()), Status.WATER_STILL, table, s, maxY);
            }
        }
    }
    
    static boolean isSupported(Pair<Integer, Integer> p, Table<Integer, Integer, Status> t)
    {
        Map<Integer, Status> same = t.column(p.getValue());
        Map<Integer, Status> below = t.column(p.getValue()+1);
        
        boolean isSupported = true;
        for(int minX = p.getKey(); isSupported; minX--)
        {
            Status rBelow = below.get(minX);
            Status actual = same.get(minX);
        
            if (actual == Status.CLAY)
                break; // isOk
        
            if (rBelow != Status.CLAY && rBelow != Status.WATER_STILL)
                isSupported = false;
        }
        for(int maxX = p.getKey(); isSupported; maxX++)
        {
            Status rBelow = below.get(maxX);
            Status actual = same.get(maxX);
        
            if (actual == Status.CLAY)
                break; // isOk
        
            if (rBelow != Status.CLAY && rBelow != Status.WATER_STILL)
                isSupported = false;
        }
        
        return isSupported;
    }
    
    static void displaySurroundings(Table<Integer, Integer, Status> t, int x, int y)
    {
        displaySurroundings(t, x-5, x+5, y-5, y+5);
    }

    static void displaySurroundings(Table<Integer, Integer, Status> t, int minX, int maxX, int minY, int maxY)
    {
        StringBuilder sb = new StringBuilder();

        for (int currY = Math.max(minY, 0); currY <= maxY; currY++)
        {
            Map<Integer, Status> currRow = t.column(currY);
            for (int currX = minX; currX <= maxX; currX++)
            {
                Status place = currRow.get(currX);
                if (place == null)
                    place = Status.SAND;

                switch (place)
                {
                    case SAND:
                        sb.append(" ");
                        break;
                    case CLAY:
                        sb.append("#");
                        break;
                    case WATER_STILL:
                        sb.append("~");
                        break;
                    default:
                        sb.append("|");
                }
            }
            sb.append("\n");
        }

        sb.append(minX).append(',').append(minY).append(":").append(maxX).append(",").append(maxY).append("\n");

        System.out.println(sb.toString());
    }
}
