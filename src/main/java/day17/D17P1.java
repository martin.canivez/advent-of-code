package day17;

import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;
import day17.D17Util.Status;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static day17.D17Util.process;

public class D17P1 {
    public static void main(String[] args) throws IOException, InterruptedException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day17/input.txt")));
        Table<Integer, Integer, Status> table = TreeBasedTable.create();

        for (String e : data)
        {
            String coords[] = e.split(", ");
            char one = coords[0].charAt(0);
            int coordOne = Integer.parseInt(coords[0].substring(2));

            coords = coords[1].substring(2).split("\\.\\.");

            int coordTwoS = Integer.parseInt(coords[0]);
            int coordTwoE = Integer.parseInt(coords[1]);

            for (int currTwo = coordTwoS; currTwo <= coordTwoE; currTwo++)
            {
                table.put(one == 'x' ? coordOne : currTwo, one == 'x' ? currTwo : coordOne, Status.CLAY);
            }
        }

        int minX = table.columnMap().values().stream().flatMap(r -> r.keySet().stream()).min(Integer::compareTo).orElseThrow(RuntimeException::new);
        int maxX = table.columnMap().values().stream().flatMap(r -> r.keySet().stream()).max(Integer::compareTo).orElseThrow(RuntimeException::new);

        int maxY = table.columnKeySet().stream().max(Integer::compareTo).orElseThrow(RuntimeException::new);
        int minY = table.columnKeySet().stream().min(Integer::compareTo).orElseThrow(RuntimeException::new);

        for (int i = 0; i <= maxY; i++)
        {
            Map<Integer, Status> col = table.column(i);
            for (int currX = minX-1; currX <= maxX+1; currX++)
                col.putIfAbsent(currX, Status.SAND);
        }
        
        process(table);
    
        System.out.println(table.columnMap()
                                .entrySet()
                                .stream()
                                .filter(e -> e.getKey() >= minY && e.getKey() <= maxY)
                                .map(Map.Entry::getValue).flatMap(m -> m.values().stream())
                                .filter(s -> s == Status.WATER_STILL || s == Status.WATER_FLOW).count());
    }
}
