package day18;

import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;
import javafx.scene.control.Tab;
import javafx.util.Pair;
import org.apache.commons.io.IOUtils;
import sun.reflect.generics.tree.Tree;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static day18.D18Util.displaySurroundings;

public class D18P2 {
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day18/input.txt")));

        Table<Integer, Integer, Character> forest = TreeBasedTable.create();

        Map<Table<Integer, Integer, Character>, Long> corrMap = new HashMap<>();

        for (int y = 0; y < data.size(); y++)
        {
            String row = data.get(y);
            for (int x = 0; x < row.length(); x++)
            {
                char c = row.charAt(x);

                forest.put(x, y, c);
            }
        }

        for (long r = 1; r <= 1000000000L; r++)
        {
            if (!corrMap.containsKey(forest)) {
                Table<Integer, Integer, Character> nf = TreeBasedTable.create();
                nf.putAll(forest);
                corrMap.put(nf, r);
                D18Util.process(forest);
            }
            else
            {
                long refRound = corrMap.get(forest);
                long cycle = r-refRound;
    
                if (r+cycle <= 1000000000L)
                {
                    while (r+cycle <= 1000000000L)
                        r += cycle;
                    r--;
                }
                else
                    D18Util.process(forest);
            }
        }

        long lumber = forest.cellSet().stream().filter(c -> c.getValue() != null && c.getValue() == '#').count();
        long tree = forest.cellSet().stream().filter(c -> c.getValue() != null && c.getValue() == '|').count();

        System.out.println(lumber + " * " + tree);
        System.out.println(lumber*tree);
    }
}
