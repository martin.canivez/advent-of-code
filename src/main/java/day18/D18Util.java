package day18;

import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;
import day17.D17Util;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class D18Util {

    static void process(Table<Integer, Integer, Character> forest)
    {
        Table<Integer, Integer, Character> nForest = TreeBasedTable.create();

        for (Table.Cell<Integer, Integer, Character> c : forest.cellSet())
        {
            if (c.getValue() == null || c.getColumnKey() == null || c.getRowKey() == null)
                continue;

            Character up = forest.get(c.getRowKey(), c.getColumnKey()-1);
            Character down = forest.get(c.getRowKey(), c.getColumnKey()+1);
            Character left = forest.get(c.getRowKey()-1, c.getColumnKey());
            Character right = forest.get(c.getRowKey()+1, c.getColumnKey());
            Character upL = forest.get(c.getRowKey()-1, c.getColumnKey()-1);
            Character downR = forest.get(c.getRowKey()+1, c.getColumnKey()+1);
            Character leftD = forest.get(c.getRowKey()-1, c.getColumnKey()+1);
            Character rightU = forest.get(c.getRowKey()+1, c.getColumnKey()-1);

            List<Character> adj = Arrays.asList(up, down, left, right, upL, downR, leftD, rightU);

            switch (c.getValue()) {
                case '#':
                    nForest.put(c.getRowKey(), c.getColumnKey(), (adj.stream().anyMatch(a -> a != null && a == '|') && adj.stream().anyMatch(a -> a != null && a == '#') ? '#' : '.'));
                    break;
                case '|':
                    nForest.put(c.getRowKey(), c.getColumnKey(), adj.stream().filter(a -> a != null && a == '#').count() >= 3 ? '#' : '|');
                    break;
                case '.':
                    nForest.put(c.getRowKey(), c.getColumnKey(), adj.stream().filter(a -> a != null && a == '|').count() >= 3 ? '|' : '.');
                    break;
            }
        }

        forest.clear();
        forest.putAll(nForest);
    }

    static void displaySurroundings(Table<Integer, Integer, Character> t)
    {
        StringBuilder sb = new StringBuilder();

        for (int currY = 0; currY <= t.rowKeySet().stream().max(Integer::compareTo).orElseThrow(RuntimeException::new); currY++)
        {
            Map<Integer, Character> currRow = t.column(currY);
            for (int currX = 0; currX <= t.columnKeySet().stream().max(Integer::compareTo).orElseThrow(RuntimeException::new); currX++)
            {
                Character place = currRow.get(currX);
                if (place == null)
                    place = ' ';

                sb.append(place);
            }
            sb.append("\n");
        }

        System.out.println(sb.toString());
    }
}
