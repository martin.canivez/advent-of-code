package day18;

import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static day18.D18Util.displaySurroundings;

public class D18P1 {
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day18/input.txt")));

        Table<Integer, Integer, Character> forest = TreeBasedTable.create();

        for (int y = 0; y < data.size(); y++)
        {
            String row = data.get(y);
            for (int x = 0; x < row.length(); x++)
            {
                char c = row.charAt(x);

                forest.put(x, y, c);
            }
        }

        for (int r = 0; r < 10; r++)
        {
            D18Util.process(forest);
        }

        long lumber = forest.cellSet().stream().filter(c -> c.getValue() != null && c.getValue() == '#').count();
        long tree = forest.cellSet().stream().filter(c -> c.getValue() != null && c.getValue() == '|').count();

        System.out.println(lumber + " * " + tree);
        System.out.println(lumber*tree);
    }
}
