package day13;

import org.apache.commons.io.IOUtils;
import org.apache.commons.math3.complex.Complex;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import static day13.D13Util.*;

public class D13P2
{
    public static void main(String[] args) throws IOException {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day13/input.txt")));

        List<Cart> carts = new ArrayList<>();

        Map<Complex, Track> trackMap = buildTerrain(data, carts);

        while (true)
        {
            carts.sort(Cart::compare);
            List<Cart> newCarts = new ArrayList<>(carts);

            for (Cart c : carts)
            {
                if (!newCarts.contains(c))
                    continue;

                c.forward();
                Track nextTrack = trackMap.getOrDefault(c.pos, Track.NONE);

                if (nextTrack == Track.NONE)
                    throw new RuntimeException(c.toString());

                Optional<Cart> optionalCart = carts.stream().filter(lc -> lc != c && lc.pos.equals(c.pos)).findFirst();

                if (optionalCart.isPresent())
                {
                    newCarts.removeAll(Arrays.asList(c, optionalCart.get()));

                    if (newCarts.size() == 1) {
                        System.out.println((int)(newCarts.get(0).x()) + "," + (int)(newCarts.get(0).y()));
                        return;
                    }

                    continue;
                }

                processTurn(c, nextTrack);
            }

            carts = newCarts;
        }
    }
}
