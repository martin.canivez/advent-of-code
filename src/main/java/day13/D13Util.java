package day13;

import org.apache.commons.math3.complex.Complex;

import java.util.*;
import java.util.stream.Collectors;

public class D13Util
{
    static final Complex leftTurn = new Complex(0, -1);
    static final Complex rightTurn = new Complex(0, 1);
    static final Complex straight = new Complex(1, 0);

    static class Cart
    {
        private static int idCounter = 0;

        int id;
        int nbTurns;
        Complex pos;
        Complex direction;

        public Cart(Complex position, Complex direction) {
            this.id = idCounter++;
            this.pos = position;
            this.direction = direction;
        }

        public void addTurn(int nbTurns)
        {
            this.nbTurns = (this.nbTurns + nbTurns) % 3;
        }

        public void addTurn()
        {
            addTurn(1);
        }

        public void forward() {
            pos = pos.add(direction);
        }

        public double x()
        {
            return pos.getReal();
        }

        public double y()
        {
            return pos.getImaginary();
        }

        public static int compare(Cart c1, Cart c2)
        {
            return c1.y() == c2.y() ? Double.compare(c1.x(), c2.x()) : Double.compare(c1.y(), c2.y());
        }

        @Override
        public String toString() {
            return "Cart{" +
                    "id=" + id +
                    ", pos=" + pos +
                    ", nbTurns=" + nbTurns +
                    ", direction=" + direction +
                    '}';
        }
    }

    enum Track
    {
        NONE(' '),
        HORIZONTAL('-'),
        VERTICAL('|'),
        CROSS('+'),
        RIGHT_SLOPE('\\'),
        LEFT_SLOPE('/');

        private final char trackString;

        Track(char trackString) {
            this.trackString = trackString;
        }

        public char trackString() {
            return trackString;
        }
    }

    static Map<Complex, Track> buildTerrain(List<String> in, List<Cart> cartList)
    {
        Map<Complex, Track> out = new HashMap<>();

        for (int y = 0; y < in.size(); y++)
        {
            String row = in.get(y);
            for (int x = 0; x < row.length(); x++)
            {
                char c = row.charAt(x);

                if (c == '>' || c == '<')
                {
                    cartList.add(new Cart(new Complex(x, y), new Complex(c == '>' ? 1 : -1, 0)));
                    c = '-';
                }
                else if (c == '^' || c == 'v')
                {
                    cartList.add(new Cart(new Complex(x, y), new Complex(0, c == 'v' ? 1 :-1)));
                    c = '|';
                }

                Track currentTrack = getTrack(c);

                if (currentTrack != Track.NONE)
                    out.put(new Complex(x, y), currentTrack);
            }
        }

        return out;
    }

    static Track getTrack(char c)
    {
        List<Track> fit = Arrays.stream(Track.values()).filter(t -> t.trackString() == c).collect(Collectors.toList());

        if (fit.size() == 1)
            return fit.get(0);

        return Track.NONE;
    }

    static void processTurn(Cart c, Track nextTrack)
    {
        boolean horizontal = c.direction.getImaginary() == 0;

        if (nextTrack == Track.CROSS)
        {
            int turn = c.nbTurns;
            c.addTurn();

            Complex cTurn = turn == 0 ? leftTurn : turn == 2 ? rightTurn : straight;

            c.direction = c.direction.multiply(cTurn);
        }
        else if (nextTrack == Track.LEFT_SLOPE || nextTrack == Track.RIGHT_SLOPE)
            c.direction = c.direction.multiply((horizontal ^ nextTrack == Track.RIGHT_SLOPE) ? leftTurn : rightTurn);
    }
}
