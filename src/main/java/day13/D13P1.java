package day13;

import org.apache.commons.io.IOUtils;
import org.apache.commons.math3.complex.Complex;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static day13.D13Util.*;

public class D13P1
{
    public static void main(String[] args) throws IOException {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day13/input.txt")));

        List<Cart> carts = new ArrayList<>();

        Map<Complex, D13Util.Track> trackMap = buildTerrain(data, carts);

        while (true)
        {
            carts.sort(Cart::compare);

            for (Cart c : carts)
            {
                c.forward();
                Track nextTrack = trackMap.getOrDefault(c.pos, Track.NONE);

                if (nextTrack == Track.NONE)
                    throw new RuntimeException(c.toString());

                if (carts.stream().anyMatch(lc -> lc != c && lc.pos.equals(c.pos)))
                {
                    System.out.println((int)(c.x()) + "," + (int)(c.y()));
                    return;
                }

                processTurn(c, nextTrack);
            }
        }
    }
}
