package day9;

import java.util.HashMap;
import java.util.Map;

public class D9Util
{
    static class Marble
    {
        public Marble(int number, Marble prev)
        {
            this.number = number;
            this.prev = prev;
            this.next = prev.next;

            prev.next = this;
            next.prev = this;
        }

        public Marble (int number, boolean single)
        {
            this.number = number;
            this.next = this;
            this.prev = this;
        }

        public void remove()
        {
            if (prev != null && prev != this)
                prev.next = next;

            if (next != null && next != this)
                next.prev = prev;

            next = null;
            prev = null;
        }

        public int number;
        public Marble prev;
        public Marble next;
    }

    static Map<Integer, Long> computeScores(int nPlayers, int nMarbles)
    {
        Map<Integer, Long> score = new HashMap<>();

        for (int i = 0; i < nPlayers; i++)
            score.put(i, 0L);

        Marble cMarble = null;
        int cPlayer = 0;

        for (int i = 0; i < nMarbles; i++) {
            if (cMarble == null) {
                cMarble = new D9Util.Marble(i, true);
            } else if (i % 23 == 0) {
                int scoreToAdd = i;

                for (int j = 0; j < 6; j++)
                    cMarble = cMarble.prev;

                D9Util.Marble toRemove = cMarble.prev;
                scoreToAdd += toRemove.number;

                toRemove.remove();

                int finalScoreToAdd = scoreToAdd;
                score.computeIfPresent(cPlayer, (k, n) -> n + finalScoreToAdd);
            } else {
                cMarble = new D9Util.Marble(i, cMarble.next);
            }

            cPlayer = (cPlayer + 1) % nPlayers;
        }

        return score;
    }
}
