package day9;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D9P1
{
    public static void main(String[] args) throws IOException {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day9/input.txt")));
        Pattern p = Pattern.compile("([0-9]+) players; last marble is worth ([0-9]+) points");
        Matcher m = p.matcher(data.get(0));
        m.matches();

        int nPlayers = Integer.parseInt(m.group(1));
        int nMarbles = Integer.parseInt(m.group(2));

        Map<Integer, Long> score = D9Util.computeScores(nPlayers, nMarbles);

        System.out.println(score.values().stream().max(Long::compareTo).orElseThrow(RuntimeException::new));
    }
}
