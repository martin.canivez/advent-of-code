package day8;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class D8Util {
    //Stolen from https://stackoverflow.com/questions/3522454/java-tree-data-structure
    static class Tree<T> {
        public Node<T> root;

        public Tree(T rootData)
        {
            root = new Node<>();
            root.data = rootData;
            root.children = new ArrayList<>();
        }

        public Tree(Node<T> root)
        {
            this.root = root;
        }
    }

    //Stolen from https://stackoverflow.com/questions/3522454/java-tree-data-structure
    static class Node<T> {
        public T data;
        public Node<T> parent;
        public List<Node<T>> children;
    }

    static Tree<List<Integer>> buildTree(Deque<Integer> entries)
    {
        return new Tree<>(buildNode(entries, null));
    }

    static Node<List<Integer>> buildNode(Deque<Integer> entries, Node<List<Integer>> parent)
    {
        Node<List<Integer>> node = new Node<>();

        int childNodes = entries.removeFirst();
        int mdAmount = entries.removeFirst();

        List<Node<List<Integer>>> children = new ArrayList<>();
        List<Integer> metadata = new ArrayList<>();

        for (int i = 0; i < childNodes; i++)
            children.add(buildNode(entries, node));

        for (int i = 0; i < mdAmount; i++)
            metadata.add(entries.removeFirst());

        node.children = children;
        node.data = metadata;
        node.parent = parent;

        return node;
    }
}
