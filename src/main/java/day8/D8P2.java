package day8;

import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static day8.D8Util.buildTree;

public class D8P2 {
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day8/input.txt")));
        Deque<Integer> nbrs = Arrays.stream(data.get(0).split(" ")).map(Integer::parseInt).collect(Collectors.toCollection(LinkedList::new));

        D8Util.Tree<List<Integer>> tree = buildTree(nbrs);

        System.out.println(nodeValue(tree.root));
    }

    static int nodeValue(D8Util.Node<List<Integer>> node)
    {
        if (node.children.size() == 0)
            return node.data.stream().reduce(0, Integer::sum);

        int val = 0;

        for (Integer d : node.data)
        {
            if (node.children.size() < d)
                continue;

            val += nodeValue(node.children.get(d-1));
        }

        return val;
    }
}
