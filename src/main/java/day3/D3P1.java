package day3;

import org.apache.commons.io.IOUtils;

import java.awt.geom.Rectangle2D;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D3P1
{
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day3/input.txt")));
    
        Pattern extractor = Pattern.compile("#([0-9]+) @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)");
    
        Set<Rectangle2D> rectMap = new HashSet<>();
        
        int claimed[][] = new int[1000][1000];
        
        for (String currDat : data)
        {
            Matcher matcher = extractor.matcher(currDat);
            matcher.matches();
            
            rectMap.add(new Rectangle2D.Double(Integer.parseInt(matcher.group(2)),
                                               Integer.parseInt(matcher.group(3)),
                                               Integer.parseInt(matcher.group(4)),
                                               Integer.parseInt(matcher.group(5))));
        }
    
        for (Rectangle2D curr : rectMap)
            for (int i = (int)curr.getX(); i < (int)curr.getX() + curr.getWidth(); i++)
                for (int j = (int)curr.getY(); j < (int)curr.getY() + curr.getHeight(); j++)
                    claimed[i][j] += 1;
                
        int total = 0;
        
        for (int[] row : claimed)
            for (int cell : row)
                total += cell > 1 ? 1 : 0;
        
        System.out.println(total);
    }
}