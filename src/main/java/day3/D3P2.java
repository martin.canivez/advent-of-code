package day3;

import org.apache.commons.io.IOUtils;

import java.awt.geom.Rectangle2D;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D3P2
{
    public static void main(String[] args) throws IOException
    {
        List<String> data = new ArrayList<>(IOUtils.readLines(new FileReader("./src/main/java/day3/input.txt")));
    
        Pattern extractor = Pattern.compile("#([0-9]+) @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)");
    
        Map<Integer, Rectangle2D> rectMap = new HashMap<>();
        
        double total = 0;
        
        for (String currDat : data)
        {
            Matcher matcher = extractor.matcher(currDat);
            matcher.matches();
            
            rectMap.put(Integer.parseInt(matcher.group(1)),
                        new Rectangle2D.Double(Integer.parseInt(matcher.group(2)),
                                               Integer.parseInt(matcher.group(3)),
                                               Integer.parseInt(matcher.group(4)),
                                               Integer.parseInt(matcher.group(5))));
        }
        
        for (Map.Entry<Integer, Rectangle2D> orEntry : rectMap.entrySet())
        {
            if (!overlaps(orEntry.getValue(), rectMap))
                System.out.println(orEntry);
        }
    }
    
    private static boolean overlaps(Rectangle2D original, Map<Integer, Rectangle2D> comp)
    {
        for (Map.Entry<Integer, Rectangle2D> cmpEntry : comp.entrySet())
        {
            if (original == cmpEntry.getValue())
                continue;
            
            if (original.intersects(cmpEntry.getValue()))
                return true;
        }
        
        return false;
    }
}